package com.truckonline.api.exam.service.impl;

import java.time.Instant;
import java.util.Objects;

import com.truckonline.api.exam.dto.DriverActivityInfo;

public class TotalTmp {

	DriverActivityInfo lastActivity;
	Instant unaccountedStartDate;
	long unaccountedRest = 0;
	long unaccountedKms;
	long unaccountedNoData = 0;

	public TotalTmp() { /* */ }

	public void addUnaccountedRest(long duration){
		this.unaccountedRest += duration;
	}

	public void addUnaccountedKms(long kms){
		this.unaccountedKms += kms;
	}

	public void addUnaccountedNoData(long noData){
		this.unaccountedNoData += noData;
	}

	public void setUnaccountedStartDate(Instant unaccountedStartDate) {
		if( this.unaccountedStartDate == null )
			this.unaccountedStartDate = unaccountedStartDate;
	}

	public long getLongBreak(){
		return this.unaccountedNoData + this.unaccountedRest;
	}

	public void reset(){
		this.unaccountedRest = 0;
		this.unaccountedStartDate = null;
		this.unaccountedKms = 0;
		this.unaccountedNoData = 0;
	}

	public long getUnaccountedRest() {
		return this.unaccountedRest;
	}

	public void setUnaccountedRest(long unaccountedRest) {
		this.unaccountedRest = unaccountedRest;
	}

	public DriverActivityInfo getLastActivity() {
		return this.lastActivity;
	}

	public void setLastActivity(DriverActivityInfo lastActivity) {
		this.lastActivity = lastActivity;
	}

	public TotalTmp unaccountedRest(long unaccountedRest) {
		setUnaccountedRest(unaccountedRest);
		return this;
	}

	public TotalTmp lastActivity(DriverActivityInfo lastActivity) {
		setLastActivity(lastActivity);
		return this;
	}

	public Instant getUnaccountedStartDate() {
		return this.unaccountedStartDate;
	}

	public TotalTmp unaccountedStartDate(Instant unaccountedStartDate) {
		setUnaccountedStartDate(unaccountedStartDate);
		return this;
	}

	public long getUnaccountedKms() {
		return this.unaccountedKms;
	}

	public void setUnaccountedKms(long unaccountedKms) {
		this.unaccountedKms = unaccountedKms;
	}

	public TotalTmp unaccountedKms(long unaccountedKms) {
		setUnaccountedKms(unaccountedKms);
		return this;
	}

	public long getUnaccountedNoData() {
		return this.unaccountedNoData;
	}

	public void setUnaccountedNoData(long unaccountedNoData) {
		this.unaccountedNoData = unaccountedNoData;
	}

	public TotalTmp unaccountedNoData(long unaccountedNoData) {
		setUnaccountedNoData(unaccountedNoData);
		return this;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof TotalTmp)) {
			return false;
		}
		TotalTmp totalTmp = (TotalTmp) o;
		return unaccountedRest == totalTmp.unaccountedRest && Objects.equals(lastActivity, totalTmp.lastActivity) && Objects.equals(unaccountedStartDate, totalTmp.unaccountedStartDate);
	}

	@Override
	public int hashCode() {
		return Objects.hash(unaccountedRest, lastActivity, unaccountedStartDate);
	}

	@Override
	public String toString() {
		return "{" +
			" unaccountedRest='" + getUnaccountedRest() + "'" +
			", lastActivity='" + getLastActivity() + "'" +
			", unaccountedStartDate='" + getUnaccountedStartDate() + "'" +
			"}";
	}

}
