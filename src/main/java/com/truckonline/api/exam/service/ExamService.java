package com.truckonline.api.exam.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.truckonline.api.exam.dto.AmplitudeInfo;
import com.truckonline.api.exam.dto.RequestInfo;

/**
 * ExamService
 */
@Path("/")
public interface ExamService {

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<AmplitudeInfo> computeAmplitudes(RequestInfo activities);

}
