package com.truckonline.api.exam.service.impl;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import javax.ejb.Stateless;

import com.truckonline.api.exam.dto.AmplitudeInfo;
import com.truckonline.api.exam.dto.Constants.AMPLITUDE_TYPE;
import com.truckonline.api.exam.dto.Constants.DRIVER_ACTIVITY;
import com.truckonline.api.exam.dto.DriverActivityInfo;
import com.truckonline.api.exam.dto.RequestInfo;
import com.truckonline.api.exam.service.ExamService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ExamServiceImpl
 */
@Stateless
public class ExamServiceImpl implements ExamService {

	private static final int LONG_BREAK_PERIOD_MIN = 540;

	private static final Logger logger = LoggerFactory.getLogger(ExamServiceImpl.class);

	@Override
	public List<AmplitudeInfo> computeAmplitudes(RequestInfo activities) {

		// Init amplitude list with a service period.
		LinkedList<AmplitudeInfo> result = new LinkedList<>();
		result.add(new AmplitudeInfo().type(AMPLITUDE_TYPE.SERVICE_PERIOD).totalKm(0));

		final TotalTmp tmp = new TotalTmp();

		activities.activities.forEach( a -> {

			AmplitudeInfo lastOne = result.getLast();

			// Skip REST until a service activity is found.
			if( lastOne.getStartDate() == null && a.getActivity() == DRIVER_ACTIVITY.REST )
				return;

			logger.info("{} - {} > {}", a.getActivity(), a.getStartDate(), a.getEndDate());

			// First activity in the list.
			if( lastOne.getStartDate() == null ) {
				lastOne.setStartDate(a.getStartDate());
				lastOne.setEndDate(a.getEndDate());
			}

			// Détection des coupures longues.
			if( a.getActivity()  != DRIVER_ACTIVITY.REST && tmp.getLongBreak() >= LONG_BREAK_PERIOD_MIN){

				logger.info(" > Coupure longue détectée : {}", a.getStartDate());

				result.add(new AmplitudeInfo().type(AMPLITUDE_TYPE.LONG_BREAK_PERIOD)
												.totalKm(tmp.getUnaccountedKms())
												.startDate(tmp.getUnaccountedStartDate())
												.endDate(a.getStartDate())
												.totalRestMin(tmp.getUnaccountedRest())
												.totalNoDataMin(tmp.getUnaccountedNoData()));
				tmp.reset();

				result.add(new AmplitudeInfo().type(AMPLITUDE_TYPE.SERVICE_PERIOD).totalKm(0).startDate(a.getStartDate()).endDate(a.getEndDate()));
				lastOne = result.getLast();

			}

			long duration = ChronoUnit.MINUTES.between(a.getStartDate(), a.getEndDate());
			long kms = (long)a.getEndKm() - a.getStartKm() ;

			boolean updateLastAmplitude = true;

			switch( a.getActivity() ){

				case DRIVING:
					lastOne.setTotalDrivingMin( lastOne.getTotalDrivingMin() + duration );
					break;

				case WORK:
					lastOne.setTotalWorkMin( lastOne.getTotalWorkMin() + duration );					
					break;

				case AVAILABILITY:
					lastOne.setTotalAvailabilityMin( lastOne.getTotalAvailabilityMin() + duration );					
					break;

				case OTHER:
					lastOne.setTotalOtherMin( lastOne.getTotalOtherMin() + duration );
					break;

				case REST:
					// Do not include in last amplitude, as it is possible to be the start of a long break.
					updateLastAmplitude = false;
					tmp.setUnaccountedStartDate(a.getStartDate());
					tmp.addUnaccountedRest(duration);
					tmp.addUnaccountedKms(a.getEndKm() - a.getStartKm());
					break;

				default:
					break;

			}

			// Add current counters to last service amplitude.
			if( updateLastAmplitude ){
				lastOne.setTotalServiceMin( lastOne.getTotalServiceMin() + duration );
				lastOne.setTotalKm(lastOne.getTotalKm() + kms + tmp.getUnaccountedKms());
				lastOne.setEndDate(a.getEndDate());
				lastOne.setTotalRestMin(lastOne.getTotalRestMin() + tmp.getUnaccountedRest());
				lastOne.setTotalNoDataMin(lastOne.getTotalNoDataMin() + tmp.getUnaccountedNoData());

				// Reset unaccounted datas.
				tmp.reset();
			}			

			// Add empty period between activities.
			if( tmp.getLastActivity() != null )
				tmp.addUnaccountedNoData(ChronoUnit.MINUTES.between(tmp.getLastActivity().getEndDate(), a.getStartDate()));

			// Save last activity.
			tmp.setLastActivity(a);

		});

		return result;

	}
}
